
static void ExampleA()
{
    Console.WriteLine("\nExample A");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("Enter a word: ");
    string? word1 = Console.ReadLine();

    Console.WriteLine("Enter a second word: ");
    string? word2 = Console.ReadLine();

    Console.WriteLine("Enter a third word: ");
    string? word3 = Console.ReadLine();

    // String Join()
    string message = "Your three words are: " + string.Join(" + ", word1, word2, word3);
    Console.WriteLine(message);
}


// Puzzle A - Totally Baked
// 
// Write a program that asks "Lets bake something! What 3 ingredients do we need?: "
// Get 3 responses from the user
// Using string.Join() output "Shopping List: {ingredient1}/{ingredient2}/{ingredient3}"
/*
static void PuzzleA()
{
    Console.WriteLine("\nPuzzle A");
    Console.WriteLine("~~~~~~~~~");

}
*/


static void ExampleB()
{
    Console.WriteLine("\nExample B");
    Console.WriteLine("~~~~~~~~~");

    string order = "Shopping Cart Contains: lubricant, bead necklace, asprin";
    Console.WriteLine(order);
    Console.WriteLine("Lets be honest, you don't need the asprin... What should we swap it with?: ");
    string? input = Console.ReadLine();

    Console.WriteLine(order.Replace("asprin", input));
}


// Puzzle B - Perks of the job
// 
// Write a program that asks the user for an item to be placed in their shopping cart
// "What item would you like to be in your shopping cart?: "
// Then tell the user their item is out of stock
// "Sorry, {userItem} is out of stock. I know you can see it, but it's been bought by someone else."
// Then ask the user to replace their item with something else.
// "What can we swap {userItem} for instead?: "
// Finally, display the shopping cart with their swapped item "Shopping Cart: {swappedItem}"
/*
static void PuzzleB()
{
    Console.WriteLine("\nPuzzle B");
    Console.WriteLine("~~~~~~~~~");

}
*/


static void ExampleC()
{
    Console.WriteLine("\nExample C");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("You: *Opens Bottle*");
    Console.WriteLine("...SHAZAM!!... A Magic Geneie appears!...");
    Console.WriteLine("Genie: I am a Genie, thanks for freeing me!");

    Console.WriteLine("\nGenie: You can have 3 wishes, what will they be?");

    Console.WriteLine("Wish 1:");
    string? wish1 = Console.ReadLine();

    Console.WriteLine("Wish 2:");
    string? wish2 = Console.ReadLine();

    Console.WriteLine("Wish 3:");
    string? wish3 = Console.ReadLine();

    Console.WriteLine("\nGenie: Sure! *snaps fingers*");
    Console.WriteLine($"Your wishes: \n {wish1} \n {wish2} \n {wish3}");
    Console.WriteLine("\n ALL CAME TRUE!");

}


// Puzzle C - Spice World
//
// Write a program like the Genie example above, but with a lazy genie that only gives 1 wish
// The genie will ask to replace the wish with an easier item.
//  "Can I replace "{wish}" with a Spice Girls CD instead?"
// Then no matter what the user inputs, the genie will say
//  "You need to spice up your life, mate"
/*
static void PuzzleC()
{
    Console.WriteLine("\nPuzzle C");
    Console.WriteLine("~~~~~~~~~");

}
*/



// Run the puzzles

ExampleA();
//PuzzleA();

ExampleB();
//PuzzleB();

ExampleC();
//PuzzleC();



Console.WriteLine("\n Press enter to exit the program...");
Console.ReadLine();                                         // Keeps the console app window open until you hit enter
